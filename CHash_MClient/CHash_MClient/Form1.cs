﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace CHash_MClient
{
    public partial class Form1 : Form
    {

        TcpClient _client;
        byte[] _buffer = new byte[4096];

        public string ip_addr = "192.168.8.119";
        public int port_addr = 54000;
        string username = "default:";
        public Form1(string ip_a, string p_a, string user)
        {
            InitializeComponent();
            ip_addr = ip_a; // TODO : Find a better way of passing variables in
            port_addr = Convert.ToInt32(p_a);

            if(user.Length > 3) { username = user + ":"; } // : is the delimiter we will use server side

            _client = new TcpClient(); // enter if we want to specify local IP & port
        }
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            // Connect to the remote server. The IP address and port # could be
            // picked up from a settings file.
            try
            {
                _client.Connect(ip_addr, port_addr); // TODO: Handle exception if refuse to connect
                                                     // Start reading the socket and receive any incoming messages
                _client.GetStream().BeginRead(_buffer, 0, _buffer.Length,
                                                Server_MessageRecieved, null);
            }
            catch(SocketException)
            {
                MessageBox.Show("The IP Address or Port refused to connect.", "Warning",
                MessageBoxButtons.OK);

                Entry f = new Entry();
                f.Show();
                this.Hide();
            }
        }

        private void Server_MessageRecieved(IAsyncResult ar)
        {
            if(ar.IsCompleted)
            {
                // TODO: RECIEVE MESSAGE
                // we can use BeginInvoke - using this function allows us to get the message to the ListBox on the main thread
                var bytesIn = 0;
                try
                {
                    bytesIn = _client.GetStream().EndRead(ar);
                }
                catch(Exception)
                {
                    _client.Close();
                    MessageBox.Show("The IP Address or Port closed unexpectedly.", "Warning",
                    MessageBoxButtons.OK);

                    this.Invoke((MethodInvoker)delegate
                    {
                        Entry f = new Entry();
                        f.Show();
                        // close the form on the forms thread
                        this.Close();
                        this.Dispose();
                    });
                }

                if (bytesIn > 0)
                {
                    var temp = new byte[bytesIn];
                    Array.Copy(_buffer, 0, temp, 0, bytesIn);
                    var str = Encoding.ASCII.GetString(temp);
                    // send above as delegate

                    BeginInvoke((Action)(() => {
                        listBox1.Items.Add(str); // crashes without debugging and step into
                        listBox1.SelectedIndex = listBox1.Items.Count - 1;
                        // every time we add to list box its going to move down to newest item - the above line keeps it in view
                    }));
                }

                Array.Clear(_buffer, 0, _buffer.Length);

                try
                {
                    _client.GetStream().BeginRead(_buffer, 0, _buffer.Length,
                                              Server_MessageRecieved, null);
                }
                catch (Exception)
                {
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            {


                if (textBox1.Text.Length == 0)
                {
                    var disc = Encoding.ASCII.GetBytes(username + " has disconnected.");
                    _client.GetStream().Write(disc, 0, disc.Length);
                    _client.Close();

                }
                else
                {
                    aes n = new aes();

                    var msg = Encoding.ASCII.GetBytes(username + n.encrypt(textBox1.Text,true));
                    _client.GetStream().Write(msg, 0, msg.Length);
                }
            }

            textBox1.Text = ""; // clear text
            textBox1.Focus(); // return focus
        }
    }
}