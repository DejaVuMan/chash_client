﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Media;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CHash_MClient
{
    public partial class Entry : Form
    {
        public Entry()
        {
            InitializeComponent();
        }

        public void apply_Click(object sender, EventArgs e)
        {
            // TODO: assign variables which take text inputs

            // TODO: pass variables into Form1.cs

            if((ip.Text.Length < 8 && !Regex.IsMatch(ip.Text, @"\d")) || port.Text.Length < 1)
            {
                SoundPlayer simpleSound = new SoundPlayer(@"c:\Windows\Media\chord.wav"); // windows exclusive
                simpleSound.Play(); // TODO - make custom form
                MessageBox.Show("Please check the formatting of your IP Address and Port. They seem to be incorrect.", "Warning",
                MessageBoxButtons.OK);
            }
            else
            {
                Form1 f = new Form1(ip.Text, port.Text, username.Text);
                f.Show();
                f = null;
                this.Hide();
            }
        }
    }
}